package Pages;

import org.junit.rules.ExpectedException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class MainPage {
    private final Object signInButton;

    public MainPage(WebDriver driver){
        this.signInButton = driver.findElement(By.linkText("Sign in"));
    }
    public void clickSignInButton(){
        if(ExpectedConditions.elementToBeClickable(signInButton).equals(true)) {
            signInButton.click();
        }
    }
}
