import io.codearte.jfairy.Fairy;
import io.codearte.jfairy.producer.person.Person;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
public class Test2 {
    Fairy fairy = Fairy.create();
    Person person = fairy.person();
    WebDriver driver= new ChromeDriver();
    @Before
    public void setup()   {
        driver.manage().window().maximize();
        driver.get("http://automationpractice.com/index.php");
    }
    @After
    public void after()   {
        driver.quit();
    }
    @Test
    public void registration() {
        driver.findElement(By.className("login")).click();
        WebElement wait1 = new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.id("email_create")));
        String nowaOsoba = person.getEmail();
        System.out.println(nowaOsoba);
        wait1.sendKeys(nowaOsoba + Keys.ENTER);
        WebElement wait2 = new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(By.id("customer_firstname")));
        wait2.sendKeys("Harry");
        driver.findElement(By.id("customer_lastname")).sendKeys("Potter");
        driver.findElement(By.id("passwd")).sendKeys("Password123");
        driver.findElement(By.id("firstname")).sendKeys("Harry");
        driver.findElement(By.id("lastname")).sendKeys("Potter");
        driver.findElement(By.id("address1")).sendKeys("Privet Drive 4");
        driver.findElement(By.id("city")).sendKeys("Little Whinging");
        driver.findElement(By.id("postcode")).sendKeys("12345");
        Select drpState = new Select(driver.findElement(By.id("id_state")));
        drpState.selectByVisibleText("Alabama");
        driver.findElement(By.id("phone_mobile")).sendKeys("123456789");
        driver.findElement(By.id("alias")).sendKeys("1Home");
        driver.findElement(By.id("submitAccount")).click();
    }
}
}
